def is_safe(board, row, column):
    for check_index in range(column):
        if board[row][check_index] == 1:
            return False
    for r, c in zip(range(row, -1, -1), range(column, -1, -1)):
        if board[r][c] == 1:
            return False
    for r, c in zip(range(row, len(board), 1), range(column, -1, -1)):
        if board[r][c] == 1:
            return False
    return True

def place_queen_in_column(board, column=0):
    if column >= len(board):
        return True
    for row in range(len(board)):
        if is_safe(board, row, column):
            board[row][column] = 1
            if place_queen_in_column(board, column + 1) == True:
                return True
            board[row][column] = 0
    return False

def create_board(size):
    board = []
    for i in range(size):
        row = [0] * size
        board.append(row)
    return board

board = create_board(6)
if place_queen_in_column(board):
    from pprint import pprint
    pprint(board, width=25)
else:
    print("No Solution")
